package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1 = new Pokemon("Pikachu", 94, 12);
    public static Pokemon pokemon2 = new Pokemon("Oddish", 100, 3);
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated

        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());

        while (pokemon1.isAlive() && pokemon2.isAlive()) {


            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
        }

    }
}
